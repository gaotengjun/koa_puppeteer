// 用于导出所有的模型
const Slider = require('./slider'),
      AgencyInfo = require('./agencyInfo'),
      RecomCourse = require('./recomCourse'),
      Collection =  require('./collection'),
      Teacher =  require('./teacher'),
      Student =  require('./student'),
      CourseTab =  require('./courseTab'),
      Course = require('./course'),
      AboutUs =  require('./aboutUs'),
      Admin = require('./admin')

module.exports = {
    Slider,
    AgencyInfo,
    RecomCourse,
    Collection,
    Teacher,
    Student,
    CourseTab,
    Course,
    AboutUs,
    Admin
}

