const seq = require('../connection/mysql_connect'),
      { STRING, INT } = require('../../config/db_type_config');

// 建表
// 使用seq示例建slider表
const Slider = seq.define('slider', {
    cid: {
        comment: 'courde ID', // 注释
        type: STRING, // 类型
        allowNull: false, // 是否允许为空
        unique: true, // 是否唯一
    },
    href: {
        comment: 'course detail page link',
        type: STRING, // 类型
        allowNull: false, // 是否允许为空
    },
    imgUrl: {
        comment: 'course image url',
        type: STRING, // 类型
        allowNull: false, // 是否允许为空
    },
    title: {
        comment: 'course name',
        type: STRING, // 类型
        allowNull: false, // 是否允许为空
    },
    imgKey: {
        comment: 'qiniu image name',
        type: STRING, // 类型
        allowNull: false, // 是否允许为空
    },
    status: {
        comment: 'slider status',
        type: INT, // 类型
        defaultValue: 1, // 默认值
        allowNull: false, // 是否允许为空
    }
});

module.exports = Slider;