const Sequelize = require('sequelize'),
      { MYSQL_CONF } = require('../../config/db_config');

const seq = new Sequelize(...MYSQL_CONF.conf, MYSQL_CONF.base);

// 同步数据库时 才执行的代码
// seq.authenticate().then(res => {
//     console.log('MySQL server is connected completely.');
// }).catch(error => {
//     console.log('MySQL server is filed to be connected. Error information is below ' + error);
// });

module.exports = seq;