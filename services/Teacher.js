const TeacherModel = require('../db/models/teacher')

class TeacherService {
    async addTeacher(data){
        const tid = data.tid;

        const res = await TeacherModel.findOne({
            where: { tid }
        });

        if(res){
            return await TeacherModel.update(data, {
                where: { tid }
            })
        } else {
            return await TeacherModel.create(data);
        }
    }

    // 获取老师数据
    async getTeacherData() {
        return await TeacherModel.findAll({
            attributes: {
                exclude: ['tid', 'teacherImg']
            }
        })
    }

    // 修改老师状态
    async changeTeacherStatus(id, status) {
        const res = await TeacherModel.update({ status }, {
            where: { id }
        });

        return res[0];
    }

    // 修改老师分类
    async selectStarTeacher(id, isStar) {
        const res = await TeacherModel.update({ isStar }, {
            where: { id }
        });
        
        return res[0];
    }
}

module.exports = new TeacherService();