const SliderModel = require('../db/models/slider');

class SliderService {
    // 添加数据
    async addSliderData(data){
        // 先存储cid
        const cid = data.cid;
        // 查询表中cid = cid的数据是否存在
        const result = await SliderModel.findOne({ // 查询cid为cid的数据
            where: { cid }
        });

        // 如果存在就更新，不存在就新增
        if(result){
            return await SliderModel.update(data, { // 更新cid为cid的数据
                where: { cid }
            });
        } else {
            return await SliderModel.create(data); // 添加数据
        }
    }

    // 获取全部轮播图数据
    async getSliderData() {
        return await SliderModel.findAll({
            attributes: {
                exclude: ['imgUrl']
            }
        })
    }

    // 修改轮播图上下架
    async changeSliderStatus(id, status) {
        const res = await SliderModel.update({ status }, {
            where: { id }
        });

        return res[0];
    }
}

// 导出类的实例
module.exports = new SliderService();