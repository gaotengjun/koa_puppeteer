const CourseModel = require('../db/models/course');

class CourseService {
    async addCourse(data){
        const cid = data.cid;

        const res = await CourseModel.findOne({
            where: { cid }
        });

        if(res){
            return await CourseModel.update(data, {
                where: { cid }
            });
        } else {
            return await CourseModel.create(data);
        }
    }

    // 获取全部课程数据
    async getCourseData(){
        return await CourseModel.findAll({
            attributes: {
                // 忽略字段
                exclude: ['posterUrl', 'description', 'createAt', 'updateAt']
            }
        });
        // return null;
    }

    
    // 修改课程分类
    async changeField(cid, field){
        const res =  await CourseModel.update({ field }, {
            where: { cid }
        });

        return res[0];
    }

    // 修改课程状态
    async changeCourseStatus(id, status) {
        const res = await CourseModel.update({ status }, {
            where: { cid: id }
        });

        return res[0];
    }
}

module.exports = new CourseService();