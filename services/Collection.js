const CollectionModel = require('../db/models/collection');

class CollectionService {
    async addCollection(data){
        const cid = data.cid;

        // 查询当前cid
        const res = await CollectionModel.findOne({
            where: { cid },
        });

        if(res){
            return await CollectionModel.update(data, {
                where: { cid },
            })
        } else {
            return await CollectionModel.create(data);
        }
    }

    // 获取课程集合数据
    async getCollectionData() {
        return await CollectionModel.findAll({
            attributes: {
                exclude: ['posterUrl', 'courseIdList', 'teacherImg']
            }
        });
    }

    // 修改课程集合上下架
    async changeCollectionStatus(id, status) {
        const res = await CollectionModel.update({ status }, {
            where: { id }
        });

        return res[0];
    }
}

module.exports = new CollectionService();