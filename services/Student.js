const StudentModel = require('../db/models/student');

class StudentService {
    async addStudent(data){
        const sid = data.sid;

        const res = await StudentModel.findOne({
            where: { sid }
        });

        if(res){
            // 更新数据
            return await StudentModel.update(data, {
                where: { sid }
            });
        } else {
            return await StudentModel.create(data);
        }

    }

    // 获取学生数据
    async getStudentData() {
        return await StudentModel.findAll({
            attributes: {
                exclude: ['studentImg']
            }
        })
    }

    // 修改学生状态
    async changeStudentStatus(id, status) {
        const res = await StudentModel.update({ status }, {
            where: { id }
        });

        return res[0];
    }
}

module.exports = new StudentService();