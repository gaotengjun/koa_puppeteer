const AgencyInfoModel = require('../db/models/agencyInfo');

// 定义操作表agency_infos的类
class AgencyInfoService {
    async addAgencyInfo(data){
        const id = 1;

        const result = await AgencyInfoModel.findOne({
            where: { id },
        });

        console.log(result);
        // 如果存在数据更新
        if(result){
            return await AgencyInfoModel.update(data, {
                where: { id },
            })
        } else {
            return await AgencyInfoModel.create(data);
        }
    }
}

module.exports = new AgencyInfoService();