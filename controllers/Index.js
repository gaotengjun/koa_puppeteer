const { getCourseData, changeField, changeCourseStatus } = require('../services/Course'),
      { getRecomCourseData, changeRecomCourseStatus } = require('../services/RecomCourse'),
      { getSliderData, changeSliderStatus } = require('../services/Slider'),
      { getCollectionData, changeCollectionStatus } = require('../services/Collection'),
      { getTeacherData, changeTeacherStatus, selectStarTeacher } = require('../services/Teacher'),
      { getStudentData, changeStudentStatus } = require('../services/Student'),
      { getCourseFieldData } = require('../services/CourseTab'),
      { returnInfo } = require('../libs/utils'),
      { API } = require('../config/error_config')

class Index {
    // 获取课程数据
    async getCourseData(ctx, next) {
        const courseData = await getCourseData(), // 课程信息
              fieldData = await getCourseFieldData(); // 课程分类信息
 
        ctx.body = courseData && fieldData // 两个数据都存在才返回
                 ? returnInfo(API.RETURN_SUCCESS, {
                    courseData,
                    fieldData
                 })
                 : returnInfo(API.RETURN_FAILED);
    }

    // 获取推荐课程数据
    async getRecomCourse(ctx, next) {
        const recomCourseData = await getRecomCourseData();

        ctx.body = recomCourseData 
                 ? returnInfo(API.RETURN_SUCCESS, recomCourseData)
                 : returnInfo(API.RETURN_FAILED);
    }

    // 获取轮播图数据
    async getSlider(ctx, next) {
        const sliderData = await getSliderData();

        ctx.body = sliderData 
                 ? returnInfo(API.RETURN_SUCCESS, sliderData)
                 : returnInfo(API.RETURN_FAILED);
    }

    // 获取课程集合数据
    async getCollections(ctx, next) {
        const collectionData = await getCollectionData();

        ctx.body = collectionData 
                 ? returnInfo(API.RETURN_SUCCESS, collectionData)
                 : returnInfo(API.RETURN_FAILED);
    }

    // 获取老师数据
    async getTeachers(ctx, next) {
        const teacherData = await getTeacherData();

        ctx.body = teacherData 
                 ? returnInfo(API.RETURN_SUCCESS, teacherData)
                 : returnInfo(API.RETURN_FAILED);
    }

    // 获取学生数据
    async getStudents(ctx, next) {
        const studentData = await getStudentData();

        ctx.body = studentData 
                 ? returnInfo(API.RETURN_SUCCESS, studentData)
                 : returnInfo(API.RETURN_FAILED);
    }
 

    // 修改课程分类
    async changeCourseField(ctx, next) {
        const { cid, field } = ctx.request.body;
        
        const res = await changeField(cid, field);

        if(!res){
            ctx.body = returnInfo(API.CHANGE_COURSE_FIELD_FAILED);
        } else {
            ctx.body = returnInfo(API.CHANGE_COURSE_FIELD_SUCCESS);
        }
    }

    // 修改老师分类
    async changeTeacherStar(ctx, next) {
        const { id, isStar } = ctx.request.body;

        const res = await selectStarTeacher(id, isStar);

        if(!res){
            ctx.body = returnInfo(API.SELECT_STAR_TEACHER_FAILED);
        } else {
            ctx.body = returnInfo(API.SELECT_STAR_TEACHER_SUCCESS);
        }
    }

    // 修改状态 公共方法
    async changeDataStatus(ctx, next) {
        const { id, status, field } = ctx.request.body;
        let res = null;

        switch(field) {
            case 'COURSE':
                res = await changeCourseStatus(id, status);
                break;
            case 'RECOM_COURSE':
                res = await changeRecomCourseStatus(id, status);
                break; 
            case 'SLIDER':
                res = await changeSliderStatus(id, status);
                break;
            case 'COLLECTION':
                res =await changeCollectionStatus(id, status);
                break;
            case 'TEACHER':
                res =await changeTeacherStatus(id, status);
                break;
            case 'STUDENT':
                res =await changeStudentStatus(id, status);
                break;
            default:
                ctx.body = returnInfo(API.FIELD_ERROR);
                return;
        }

        // 修改成功
        if(!res) {
            ctx.body = returnInfo(API.CHANGE_STATUS_FAILED);
            return;
        }

        // 修改失败
        ctx.body = returnInfo(API.CHANGE_STATUS_SUCCESS);
    }

}

module.exports = new Index();