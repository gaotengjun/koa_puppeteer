// 数据库字段类型 配置文件
const Sequelize = require('sequelize');

module.exports = {
    STRING: Sequelize.STRING, // 字符串
    INT: Sequelize.INTEGER, // 整形
    DECIMAL: Sequelize.DECIMAL, // 小数
    TEXT: Sequelize.TEXT, // 文本类型
}