module.exports = {
    // 登录提示信息
    LOGIN: {
        INVALID_USERNAME_LENGTH: { // 无效的用户名长度
            error_code: 10001,
            error_msg: 'Invalid username length.'
        },
        INVALID_PASSWORD_LENGTH: { // 无效的密码长度
            error_code: 10002,
            error_msg: 'Invalid password length.'
        },
        USERNAME_NOT_EXIST: { // 数据库中不存在用户名
            error_code: 10003,
            error_msg: 'Username is not exist in database.'
        },
        PASSWORD_ERROR: { // 数据库中用户名对应的密码错误
            error_code: 10004,
            error_msg: 'Password doesn\'t matched with the username.'
        },
        INVALID_OPERATION: { 
            error_code: 10005,
            error_msg: 'Invalid operation.' // 无效操作
        },  
        NOT_LOGIN_STATUS: {
            error_code: 10006,
            error_msg: 'It is not loged status.' // 没有登录状态
        },
        LOGIN_STATUS: {
            error_code: 10007,
            error_msg: 'It is loged status.' // 有登录状态
        },
        LOGOUT_SUCCESS: { // 退出登录成功
            error_code: 0,
            error_msg: 'Logout is ok'
        },
        SUCCESS: { // 无效的密码长度
            error_code: 0,
            error_msg: 'Login is ok.'
        },
    },
    // 接口返回信息
    API: {
        RETURN_SUCCESS: {
            error_code: 0,
            error_msg: 'Data is returned successfully'
        },
        RETURN_FAILED: {
            error_code: 20001,
            error_msg: 'It is failed to return data'
        },
        CHANGE_COURSE_FIELD_SUCCESS: { // 修改数据成功
            error_code: 0,
            error_msg: 'Changing course field successfully.'
        },
        CHANGE_COURSE_FIELD_FAILED: { // 修改数据失败
            error_code: 20002,
            error_msg: 'Is is failed to change course field.'
        },
        CHANGE_STATUS_SUCCESS: { // 修改状态成功
            error_code: 0, 
            error_msg: 'Changing status field successfully.'
        },
        CHANGE_STATUS_FAILED: { // 修改状态失败
            error_code: 20003,
            error_msg: 'Is is failed to change status field.'
        },
        FIELD_ERROR: { // field 传入的不存在
            error_code: 20004,
            error_msg: 'It is a wrong field for the operation.'
        },
        SELECT_STAR_TEACHER_SUCCESS: { // 修改老师分类状态成功
            error_code: 0, 
            error_msg: 'Selecting star teacher successfully.'
        },
        SELECT_STAR_TEACHER_FAILED: { // 修改老师分类状态失败
            error_code: 20005,
            error_msg: 'Is is failed to select star teacher.'
        },
    },
    // 爬取数据返回信息
    CRAWLER: {
        CRAWL_SUCCESS: {
            error_code: 0,
            error_msg: 'Crawling data successfully.'
        },
        CRAWL_FAILED: {
            error_code: 30001,
            error_msg: 'It is failed to crawl data.'
        }
    }
}