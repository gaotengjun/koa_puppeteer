const ENV = require('./env_config');
module.exports = {
    MYSQL_CONF: { // mysql配置
        base: {
            host: 'localhost',
            dialect: 'mysql',
            pool: { 
                max: 5,
                min: 0,
                idle: 10000 // 10s
            }
        },
        // 数据库名 用户名  密码
        conf: ['txclass', 'root', ENV.isPrd ? 'password' : 'root']
    },
    REDIS_CONF: [6479, '127.0.0.1']
}



// const Sequelize = require('sequelize');

// const seq = new Sequelize('txclass', 'root', 'root', {
//     host: 'localhost',
//     dialect: 'mysql', 
//     pool: { // 连接池 多人操作不同线程
//         max: 5, // 最大连接数
//         min: 0, // 最小连接数
//         idle: 10000 // 超时时间
//     }
// });