const crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

crawler({
    url: crawler_cof.url.main,
    callback(){
        const $ = window.$, 
              $item = $('.agency-recommend-course');
         
        let data = []; // 保存数据

        $item.each((index, item) => {
            const $el = $(item);

            const dataItem = {
                cid: index + 1,
                title: $el.find('.recommend-course-title span').eq(0).text().replace(/(\\n|\s+|更多)/g, ''), // 去除空格换行和“更多”俩字
                info: $el.find('.rec-group-info').text(), 
                qqQunLink: $el.find('.rec-group-join').prop('href'),
                // posterUrl: $el.find('.rec-group-mask').css('background-image').replace(/(url\(\"|\)|\")/g, ''),
                posterUrl: $el.find('.rec-group-mask').css('background-image').match(/\"(.+?)\"/)[1], // 使用match直接匹配双引号中的内容 取第一项
                courseIdList: '', 
                posterKey: '',
            }

            // 获取分类下的视频id
            let _idList = [];

            const $courseItem = $el.find('.course-card-item');

            $courseItem.each((index, item) => {
                const $elem = $(item);

                _idList.push($elem.find('.item-img-link').attr('data-id'));
            });

            dataItem.courseIdList = _idList.toString();

            data.push(dataItem);
        });

        return data;
    }
})