const crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

crawler({
    url: crawler_cof.url.main,
    callback(){
        const $ = window.$, 
              $section = $('.agency-head');
         
        return {
            logoUrl: $section.find('.agency-head-logo').prop('src'), // 课程logo图片地址
            name: $section.find('.ag-title-main').text(), // 课程名称
            feedbackRate: $section.find('.ag-info span').eq(0).text().replace(/[^0-9]/ig, ''),  // 好评度 // 以数字0-9开头，不区分大小写，全局匹配
            studentCount: $section.find('.js-item-num').attr('data-num'), // 学生数量
            description: $section.find('.ag-info-des').text(), // js++描述
            qqLink: $section.find('.ag-info-btn').prop('href'), // 点击跳转qq地址
            logoKey: '', // 图片上传七牛图床的名字
        };
    }
})