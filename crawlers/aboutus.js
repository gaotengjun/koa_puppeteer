const Crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

Crawler({
    url: crawler_cof.url.aboutus,
    callback(){
        const $ = window.$, 
              $wrapper = $('.agency-tab');

        const data = {
            aid: 1,
            posterUrl: $wrapper.find('.about-banner-wrap-0').css('background-image').match(/\"(.+?)\"/)[1],
            title: $wrapper.find('.about-agency-propagate').text(),
            name: $wrapper.find('.about-agency-name').text(),
            intro: $wrapper.find('.about-agency-intr').text(),
            posterImgKey: ''
        }

        console.log(data);
        return data;
    }
})