const Crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

Crawler({
    url: crawler_cof.url.course,
    field: 'course',
    callback(){
        const $ = window.$, 
              $item = $('.course-card-list li');

        // 存储爬取数据
        const data = [];	
              
        $item.each((index, item) => {
            const $el = $(item),
                  $itemLk = $el.find('.item-img-link');

            const dataItem = {
                cid: $itemLk.attr('data-id'),
                href: $itemLk.prop('href'),
                posterUrl: $itemLk.find('.item-img').prop('src'),
                courseName: $itemLk.find('.item-img').prop('title'),
                price: $el.find('.item-price').text() == '免费' ? '0' : $el.find('.item-price').text().slice(1), // 课程价格
                description: $el.find('.item-status-step').text(),
                studentCount: parseInt($el.find('.item-user').text()), // 购买数量
                posterKey: ''
            };

            data.push(dataItem);
        });

        return data;
    }
})