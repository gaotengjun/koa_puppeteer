// 操作redis方法
const red = require('../db/connection/redis_connect');


// 设置redis
function redisSet(key, value, timeout = 60 * 60){
    if(typeof value === 'object'){ // 如果值为对象，则需要转换为字符串
        value = JSON.stringify(value);
    }

    red.set(key, value); // 设置key的值
    red.expire(key, timeout); // 设置过期时间
}

// 获取redis
function redisGet(key){
    return new Promise((resolve, reject) => {
        red.get(key, (error, value) => {
            if(error){
                reject(error);
                return;
            }

            if(value == null){
                resolve(null);
            }

            try{
                resolve(JSON.parse(value));
            } catch(e){
                resolve(value)
            }
        })
    });
}

module.exports = {
    redisSet,
    redisGet
}