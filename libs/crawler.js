const pt = require('puppeteer');

module.exports = async function(options) {
    try {
        const bs = await pt.launch({
            //headless: false,
			// args: ['--no-sandbox', '--disable-setuid-sandbox'], //沙盒 沙箱
            // handleSIGINT: false,
            // ignoreDefaultArgs: ['--disable-extensions'],
            // executablePath: '/root/.chromium-browser-snapshots/linux-722234/chrome-linux/chrome'
          }), // 启动浏览器
          pg = await bs.newPage(), // 打开一个页面
          url = options.url;
    
        // 等待去url 状态 为networkidle2
        await pg.goto(url, {
            waitUntil: 'networkidle2',
            timeout: 0
        });

        const result = await pg.evaluate(options.callback);

        // 处理存在第二页数据的情况
        // if(result && options.field === 'course') {
        //     // 等待按钮出现在页面
        //     await pg.waitForSelector('.page-btn.page-last');
        //     // 等待点击按钮
        //     await pg.click('.page-btn.page-last');
        //     // 等待两秒
        //     await pg.waitFor(2000);
        //     // 爬取数据
        //     const res = await pg.evaluate(options.callback);
        //     // 等待两秒
        //     await pg.waitFor(2000);
        //     // 使用for循环合并数组，不使用concat，concat会合并不上有问题
        //     for(var i = 0; i < res.length; i++) {
        //         await result.push(res[i]);
        //     }
        // }

        await bs.close(); // 关闭浏览器

        process.send(result); // 进程将结果sned出去

        // 最后关闭进程
        setTimeout(() => {
            process.exit(0);
        }, 1000);
    } catch(err) {
        console.log(err);
    }
}