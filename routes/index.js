var router = require('koa-router')(),
    indexController = require('../controllers/Index'),
    loginCheck = require('../middlewares/loginCheck');

// 获取全部课程 和 全部分类
router.get('/get_courses', loginCheck, indexController.getCourseData);

// 获取全部推荐课程
router.get('/get_recom_courses', loginCheck, indexController.getRecomCourse);

// 获取轮播图数据
router.get('/get_sliders', loginCheck, indexController.getSlider);

// 获取课程集合数据
router.get('/get_collections', loginCheck, indexController.getCollections);

// 获取老师信息
router.get('/get_teachers', loginCheck, indexController.getTeachers);

// 获取学生信息
router.get('/get_students', loginCheck, indexController.getStudents);

// 修改课程分类
router.post('/change_course_field', loginCheck, indexController.changeCourseField);

// 修改老师分类
router.post('/select_star_teacher', loginCheck, indexController.changeTeacherStar);

router.post('/change_status', loginCheck, indexController.changeDataStatus); // 公共接口修改状态

module.exports = router;