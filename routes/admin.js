var router = require('koa-router')(),
    adminController = require('../controllers/Admin');

// 添加前缀
router.prefix('/admin')

router.get('/create_admin', adminController.createAdmin);

// 登录接口
router.post('/login_action', adminController.loginAction);

// 登录标识校验
router.get('/login_check', adminController.loginCheck);

// 退出登录
router.get('/loginout_action', adminController.logoutAction);

module.exports = router;